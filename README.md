# mediastack-api-test-assignment

Media stack API automation for Otrium assignment - Technical Part 3

I have used the https://mediastack.com/documentation api for automation and tested few negative and positive scenarios.
API Test Framework: **Postman**

## How to execute the pipeline manually

1. Navigate to your project's CI/CD > Pipelines.
2. Click on the Run Pipeline button.
3. On the Run Pipeline page: Select the "**main**" branch
4. Click "**Run Pipeline**" button
5. Click "postman_tests" pill to view the run log

After completion you will see the test summary.
